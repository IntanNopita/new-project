import React, { useState } from "react";
import { View, Text, TextInput, TouchableOpacity, StyleSheet } from "react-native";

const Hitung = () => {
    const [panjang, setPanjang] = useState("");
    const [lebar, setLebar] = useState("");
    const [tinggi, settinggi] = useState("");
    const [volume, setvolume] = useState("");
    const [luas, setluas] = useState("");

    const hitungvolume=(panjang, lebar, tinggi)=>{
      const temp = (panjang*lebar*tinggi);
      setvolume(temp);
    }
    const hitungluas=(panjang, lebar, tinggi)=>{
      const temp =  2 * ((panjang * lebar) + (panjang * tinggi) + (lebar * tinggi));
      setluas(temp);
    }

    return (
        <View style={styles.container}>
          <View style={styles.posTitle}>
          <Text style={styles.title}>Menghitung Luas Permukaan</Text>
          <Text style={styles.title}>Dan Volume Balok</Text>

        </View>

          <View style={styles.contInput}>
            <View style={styles.posInput}>
                <TextInput 
                  style={styles.input}
                  placeholder="Masukkan panjang"
                  placeholderTextColor="#0055ff"
                  onChangeText={(value)=>setPanjang(value)}
                  value={panjang}
                />
            </View>
            <View style={styles.posInput}>
                <TextInput 
                  style={styles.input}
                  placeholder="Masukkan lebar"
                  placeholderTextColor="#0055ff"
                  onChangeText={(value)=>setLebar(value)}
                  value={lebar}
                />
            </View>
             <View style={styles.posInput}>
                <TextInput 
                  style={styles.input}
                  placeholder="Masukkan Tinggi"
                  placeholderTextColor="#0055ff"
                  onChangeText={(value)=>settinggi(value)}
                  value={tinggi}
                />
            </View>
            <View style={styles.posButton}>
                <TouchableOpacity
                  style={styles.button}
                  onPress={()=>{hitungluas(panjang, lebar, tinggi)
                  {hitungvolume(panjang, lebar, tinggi)}}}
                >
                <Text style={styles.textButton}>Hitung</Text>
                </TouchableOpacity>
            </View>
          </View>
          
        <View style={styles.posOutput}>
          <Text>Volume balok : </Text>
          <Text style={styles.textOutput}>{volume}</Text>
        </View>
        <p></p>
        <View style={styles.posOutput}>
          <Text>Luas Permukaan : </Text>
          <Text style={styles.textOutput}>{luas}</Text>
        </View>
      </View>
    )
};

const styles = StyleSheet.create({
    container:{
      marginTop: 40
    },
    posTitle:{
      alignItems: 'center'
    },
    title:{
      fontSize : 18,
      fontWeight : 'bold'
    },
    contInput:{
      backgroundColor:'#4b0016',
      margin: 20,
      padding: 15,
      borderRadius: 15
    },
    posInput:{
      marginLeft : 20,
      marginRight : 20,
      marginBottom : 10,
      backgroundColor : '#a1fff8',
      paddingLeft : 10,
      paddingRight: 10,
    },
    input:{
      height : 30
    },
    posButton:{
      margin: 20,
      alignItems:'center'
    },
    button:{
      borderRadius: 5,
      width: 180,
      height: 30,
      alignItems:'center',
      backgroundColor : '#ccffff',
      justifyContent : 'center'
    },
    textButton:{
      fontWeight: 'bold',
      color: '#0066ff'
    },
    posOutput:{
      alignItems:'center'
    },
    textOutput:{
      fontSize: 30,
      fontWeight: 'bold'
    }
})

export default Hitung;